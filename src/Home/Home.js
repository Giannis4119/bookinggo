import React, { useState, useEffect } from 'react';
import axios from 'axios';
import ListElement from './ListElement/ListElement';
import './Home.css';

const Home = (props) => {
    const [searchQuery, setSearchQuery] = useState('')
    const [suggestions, setSuggestions] = useState([])
    const [loading, setLoading] = useState(false)

    useEffect(
        () => {
          if (searchQuery.length < 2) {
            setSuggestions([])
            return;
          }
            setLoading(true)
            const timeout = setTimeout(() => {
                axios.get('https://cors.io/?https://www.rentalcars.com/FTSAutocomplete.do?solrIndex=fts_en&solrRows=6&solrTerm='+searchQuery)
                .then(response => {
                    setLoading(false)
                    setSuggestions(response.data.results.docs)
                })
                .catch(err =>  console.log(err))
            
          }, 1000);
          return () => clearTimeout(timeout);
        },
        [searchQuery]
    );

    const renderSuggestions = () => {
        if (suggestions.length === 0) {
            return null
        }
        else {
            return (
                !loading ? <ul>
                                {
                                    suggestions.map(element => (
                                        <li key={element.bookingId} onClick={() => onSelectSuggestion(element.name)}>
                                            <ListElement
                                                element={element}
                                            />
                                        </li>
                                    ))
                                }
                            </ul>
                : <div className="spinner">
                    <div className="dot1"></div>
                    <div className="dot2"></div>
                </div>
            )
        }
    }
    
    const onSelectSuggestion = (value) => {
        setSearchQuery(value)
        setSuggestions([])
    }
    console.log(suggestions)
    return (
        <header>
            <nav>
                <div className="row">
                    <h1>RentalCars</h1>
                </div>
            </nav>
            <div className="hero-text-box">
                <h1>Where are you going?</h1>
                <label>Pick-up Location</label>
                <div className="AutoCompleteText">
                    <input 
                        placeholder="city, airport, station, region, district..." 
                        type="text" 
                        onChange={e => setSearchQuery(e.target.value)} value={searchQuery}  
                    />
                    {
                        renderSuggestions()
                    }
                </div>
            </div>
        </header>
    )
}

export default Home;