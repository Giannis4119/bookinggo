import React from 'react';
import './ListElement.css';

const ListElement = (props) => {
    const { name, iata, region, country, placeType } = props.element
    const iataValue = iata ? ' ('+iata+')' : ''

    let placeTypeBadge = '';
    switch (placeType) {
        case 'A':
            placeTypeBadge = <div className="airport">Airport</div>
            break;
        case 'C':
            placeTypeBadge = <div className="city">City</div>
            break;
        case 'T':
            placeTypeBadge = <div className="station">Station</div>
            break;
        case 'D':
            placeTypeBadge = <div className="district">District</div>
            break;
    
        default:
            break;
    }

    return (
        <div className="listElement">
            <div className="placeType">
                {
                    placeTypeBadge
                }
            </div>
            <div>
                <div>
                    {
                        name + iataValue
                    }    
                </div>
                {
                    (region && country) &&
                    <div>
                        <small>
                            {
                                region+','+country
                            }
                        </small>
                    </div>
                }
            </div>
        </div>
    )
}

export default ListElement;