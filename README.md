# Project Rental Cars

This project lists the available pick-up locations where users can rent their cars. All cards retrieved from RentalCars API

## Requirements

For development, you will only need Node.js installed on your environement.

### Node

[Node](http://nodejs.org/) is really easy to install & now include [NPM](https://npmjs.org/).
You should be able to run the following command after the installation procedure
below.

    $ node --version
    v8.11.3

    $ npm --version
    5.6.0

#### Node installation on Linux

    sudo apt-get install python-software-properties
    sudo add-apt-repository ppa:chris-lea/node.js
    sudo apt-get update
    sudo apt-get install nodejs

#### Node installation on Windows

Just go on [official Node.js website](http://nodejs.org/) & grab the installer.
Also, be sure to have `git` available in your PATH, `npm` might need it.

---

## Install

    $ git clone https://Giannis4119@bitbucket.org/Giannis4119/bookinggo.git
    $ cd bookinggo
    $ npm install

## Start & watch

    $ npm start

## Simple build for production

    $ npm run build

---

## Languages & tools


### JavaScript

- [React](http://facebook.github.io/react) is used for UI.
- [Axios](https://github.com/axios/axios) is used for the api calls 

